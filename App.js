import { createStackNavigator } from "@react-navigation/stack";
import React, { createContext, useContext, useEffect, useState } from "react";
import { View, ActivityIndicator } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { onAuthStateChanged } from "firebase/auth";


import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import Chat from "./screens/Chat";
import Home from "./screens/Home";
import { auth } from "./config/firebase";

const Stack = createStackNavigator();
const AuthenticationUserContext = createContext({});
const AuthenticationUserProvider = ({children}) => {
  const [ user, setUser ] = useState(null);
  return (
    <AuthenticationUserContext.Provider value={{user, setUser}}> 
    {children}
    </AuthenticationUserContext.Provider>
  )
}


function ChatStack(){
  return(
    <Stack.Navigator defaultScreenOptions={Home} >
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Chat" component={Chat} />
    </Stack.Navigator>
  )
}

function AuthStack (){
  return(
    <Stack.Navigator defaultScreenOptions={LoginScreen} screenOptions={{headerShown: false}} >
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
    </Stack.Navigator>
  )

}

function RootNavigator(){
  const { user, setUser } = useContext(AuthenticationUserContext);
  const [ loading, setLoading] = useState(true);
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, 
      async authenticatedUser => {
        authenticatedUser ? setUser(authenticatedUser) : setUser(null);
        setLoading(false);
      }
    );

    return () => unsubscribe();
    
  }, [user]);

  if(loading){
    return(
      <View s
      tyle={{
        flex:1, 
        justifyContent: 'center', 
        alignItems: 'center'}} 
      >
        <ActivityIndicator size="large" />
      </View>
    )
  }
  return(
    <NavigationContainer>
      { user ? <ChatStack /> : <AuthStack />}
      
    </NavigationContainer>
  )
}

export default function App() {
 
  return (
   
      <AuthenticationUserProvider >
        <RootNavigator />
      </AuthenticationUserProvider>
   
    
    
    // <NavigationContainer>
    //   <Stack.Navigator>
    //     <Stack.Screen name="Login" component={LoginScreen} />
    //     <Stack.Screen name="Register" component={RegisterScreen} />
    //   </Stack.Navigator>
    // </NavigationContainer>
  )
}