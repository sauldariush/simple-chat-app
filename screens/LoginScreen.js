import React, { useState } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { Input, Button } from "react-native-elements";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../config/firebase";

const LoginScreen = ({navigation}) => {
    const [ email, setEmail ] = useState('');
    const [password, setPassword] = useState('');

    const onHandleLogin = () => {
        if(email !== "" && password !== ""){
            signInWithEmailAndPassword(auth, email, password)
                .then(() => console.log("Login success"))
                .catch((err) => Alert.alert("Login error", err.message))
        }
    }
    return (
            <View style={styles.container}>
                <Input 
                placeholder="Enter your Email"
                label="Email"
                leftIcon={{ type: 'material', name: 'email'}}
                value={email}
                onChangeText={text => setEmail(text)}
                />
                <Input 
                placeholder="Enter your password"
                label="Password"
                leftIcon={{ type: 'material', name: 'lock'}}
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry
                />
                <Button title="Sign in" style={styles.button} onPress={onHandleLogin} />
                <Button title="Register" style={styles.button} onPress={()=>navigation.navigate('RegisterScreen')} />
            </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    button: {
        width: 200,
        marginTop: 10
    },
    container: {
        flex: 1,
        alignItems: "center",
        padding: 10,
        marginTop: 40

    }
})