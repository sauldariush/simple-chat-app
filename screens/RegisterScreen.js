import React, { useState } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { Input, Button } from "react-native-elements";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../config/firebase";

const RegisterScreen = () => {
    const [ name, setName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [password, setPassword] = useState('');
  

    const onHandleRegister = () => {
        if(email !== "" && password !== "") {
            createUserWithEmailAndPassword(auth, email, password)
            .then(() => console.log("Register Success"))
            .catch((err)=> Alert.alert("Register Error", err.message))
        }      
    }


    return (
            <View style={styles.container}>
                <Input 
                placeholder="Enter your name"
                label="Name"
                leftIcon={{ type: 'material', name: 'badge'}}
                value={name}
                onChangeText={text => setName(text)}
                />
                <Input 
                placeholder="Enter your Email"
                label="Email"
                leftIcon={{ type: 'material', name: 'email'}}
                value={email}
                onChangeText={text => setEmail(text)}
                />
                <Input 
                placeholder="Enter your password"
                label="Password"
                leftIcon={{ type: 'material', name: 'lock'}}
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry
                />

                <Button title="Register" onPress={onHandleRegister} style={styles.button} />
            </View>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    button: {
        width: 200,
        marginTop: 10
    },
    container: {
        flex: 1,
        alignItems: "center",
        padding: 10,
        marginTop: 40

    }
})
