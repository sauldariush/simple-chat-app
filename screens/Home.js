import React, { useEffect } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome, Entypo } from "@expo/vector-icons";
import { colors } from "react-native-elements";

const Home = () => {
    const navigation = useNavigation();

    useEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <FontAwesome name="search" size={24} color={colors.grey0} style={{marginLeft: 15}} />
            ),
            headerRight: () => (
                <FontAwesome name="home" size={24} style={{marginRight: 15}} />
            )
        })
    }, [navigation])


    return(
        <View style={styles.container} >
            <TouchableOpacity 
                onPress={() => navigation.navigate("Chat")}
                style={StyleSheet.chatButton}
            >
                <Entypo name="chat" size={24} color={colors.grey1} />
            </TouchableOpacity>
        </View>
    )
}



export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        padding: 20,

    },
    chatButton: {
        backgroundColor: colors.secondary,
        height: 100,
        width: 100,
        borderRadius: 25,
        alignItems: 'center',
        shadowColor: colors.secondary,
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.9,
        shadowRadius: 8,
        marginRight: 50,
        marginBottom: 50,
    }
});